package com.zuitt.example;

import java.util.Scanner;

public class ControlStructures {
    public  static void main(String[] args) {
        // Operators in Java
        // Arithmetic - +, -, *, /, %
        // Comparison - >, <, >=, <=, ==, !=
        // Logical- &&, ||, !
        // Assignment- =

        // Conditional Structures in Java
        // statement allows us to manipulate the flow of the code depending on the evaluation of the condition
        // Syntax: if(condition){
        // }
        int num1 = 15;
        if(num1 % 5 == 0){
            System.out.println(num1 + " is divisible by 5");
        }

        // else statement will allow us to run a task or code if the condition fails or have a falsy values

        num1 = 36;
        if (num1 % 5 == 0){
            System.out.println(num1 + " is divisible by 5");
        } else {
            System.out.println(num1 + " is not divisible by 5");
        }

        // Mini - Activity
       /* Scanner myInt = new Scanner(System.in);
        System.out.println("Input a number:");
        Integer inputNum = myInt.nextInt();

        if (inputNum % 2 == 0){
            System.out.println(inputNum + " is even!");
        } else {
            System.out.println(inputNum + " is odd!");
        }*/

        // short-circuiting
        // a short circuit happens because the result is clear even before the complete evaluation of the expression, and the resul is returned.
        int x = 15;
        int y = 0;

        if(y == 0 || x == 15 || y != 0) System.out.println(true);

        // ternary operator
        int num2 = 24;
        String result = (num2 > 0) ? Boolean.toString(true) : Boolean.toString(false); // or
        // Boolean result = (num2 > 0) ? true : false; -> same result
        System.out.println(result);

        // Switch Cases
        // control flow structures that allow one code block to be run out of many other code block
        // this is often when the input is predictable

        Scanner myInt = new Scanner(System.in);

        System.out.println("Enter a number from 1-4 to see a SM Malls in one of the four directions");
        int directionValue = myInt.nextInt();

        switch (directionValue){
            case  1:
                System.out.println("SM north EDSA");
                break;
            case 2:
                System.out.println("SM Southmall");
                break;
            case 3:
                System.out.println("SM City Taytay");
                break;
            case 4:
                System.out.println("SM Manila");
                break;
            default:
                System.out.println("Out of Range");
        }

    }
}

package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {
    public static void main (String[] args){
        Scanner myScanner = new Scanner(System.in);

        String[] fruits = {"apple", "avocado", "banana", "kiwi", "orange"};
        System.out.println("Fruits in stock: " + Arrays.toString(fruits));

        System.out.println("Which fruit would you like to get the index of?");
        String fruitName = myScanner.nextLine();

        int fruitIndex = Arrays.asList(fruits).indexOf(fruitName);
        System.out.println("The index of " + fruitName + " is: " + fruitIndex);

        ArrayList<String> friends = new ArrayList<>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));
        System.out.println("My friends are: " + friends);


        HashMap<String, Integer> items = new HashMap<>();
        items.put("toothpaste",15);
        items.put("toothbrush",20);
        items.put("soap",12);
        System.out.println("Our current inventory consists of:");
        System.out.println(items);
    }
}
